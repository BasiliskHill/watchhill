# WatchHill

## What is it?
WatchHill is a Minecraft plugin designed specifically to keep tabs on those players that your staff are suspicious of, but don't quite have enough evidence for. 
Once a player is added to the watchlist, anyone with the permission `watchhill.notify` will be given a notification when the watched player logs on.

## Why the name?
Although "WatchHill" is really just a mashup between "watchlist" and my username, it also symbolises something closer to watching the crest of a hill for invaders waiting to conquer your server.

### TODO
- Implement GUI for list, edit, add, and remove
- Add a reload command
- Add "Phrases" to the config
- Add proper "updater" script for new versions of the config and DB
- Write proper documentation
- Possibly get artwork
