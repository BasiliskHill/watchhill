package com.basiliskhill.watchhill.commands;


import com.basiliskhill.watchhill.utils.Utils;
import com.basiliskhill.watchhill.Main;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class IDCommand implements CommandExecutor {

    private final Main plugin;

    //  Initialise command
    public IDCommand (Main plugin) {
        this.plugin = plugin;

        plugin.getCommand("id").setExecutor(this);
    }


    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        FileConfiguration config = plugin.getConfig();

        //  Prevent console from running command
        if (!(sender instanceof Player)) {
            sender.sendMessage(Utils.chat("&c&lThis command cannot be run by console!"));
            return true;
        }
        Player plr = (Player) sender;

        //  Check required permission
        if (!(plr.hasPermission("watchhill.id"))) {
            plr.sendMessage(Utils.returnPermsErrorChat(plugin));
            return true;
        }

        //  Send own UUID if no other player is mentioned
        if (args.length == 0) {
            TextComponent comp = new TextComponent(Utils.chat("&7" + plr.getUniqueId().toString()));
            comp.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, plr.getUniqueId().toString()));
            comp.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(Utils.chat("&3&lClick to copy your UUID!")).create()));
            plr.spigot().sendMessage(new ComponentBuilder(Utils.chat(config.getString("Prefix.Valid") + "&3Your UUID is: ")).append(comp).create());

            return true;
        }

        //  Improper usage check
        if (args.length > 1) {
            TextComponent comp = new TextComponent(Utils.chat( config.getString("Prefix.Fail") + "&cInvalid usage! &a&nUsage:&r &3/id &8<&7player&8>"));
            comp.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/id "));
            comp.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(Utils.chat("&3&lFind the UUID of a given player!")).create()));
            plr.spigot().sendMessage(comp);

            return true;
        }

        Player argPlr = Bukkit.getPlayer(args[0]);

        //  Other player handling
        if (argPlr == null) {
            @SuppressWarnings("deprecation") OfflinePlayer offPlr = Bukkit.getOfflinePlayer(args[0]);

            if (!(offPlr.hasPlayedBefore())) {
                plr.sendMessage(Utils.chat(config.getString("Prefix.Fail") + "&cInvalid player name!"));
            } else {
                TextComponent comp = new TextComponent(Utils.chat("&7" + offPlr.getUniqueId().toString()));
                comp.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, offPlr.getUniqueId().toString()));
                comp.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(Utils.chat("&3&lClick to copy the UUID!")).create()));
                plr.spigot().sendMessage(new ComponentBuilder(Utils.chat(config.getString("Prefix.Valid") + "&3The UUID of the &coffline &3player &7" + offPlr.getName() + "&3 is ")).append(comp).create());
            }
        } else {
            TextComponent comp = new TextComponent(Utils.chat("&7" + argPlr.getUniqueId().toString()));
            comp.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, argPlr.getUniqueId().toString()));
            comp.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(Utils.chat("&3&lClick to copy the UUID!")).create()));
            plr.spigot().sendMessage(new ComponentBuilder(Utils.chat(config.getString("Prefix.Valid") + "&3The UUID of &7" + argPlr.getName() + "&3 is ")).append(comp).create());
        }

        return false;
    }
}
