package com.basiliskhill.watchhill.commands;

import com.basiliskhill.watchhill.Main;
import com.basiliskhill.watchhill.utils.DataBase;
import com.basiliskhill.watchhill.utils.Utils;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.UUID;
import java.util.logging.Level;

public class WatchListCommand implements CommandExecutor {

    private final Main plugin;

    public WatchListCommand(Main plugin) {
        this.plugin = plugin;

        plugin.getCommand("watchlist").setExecutor(this);
    }


    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        //  Prevent console from running command
        if (!(sender instanceof Player)) {
            sender.sendMessage(Utils.chat("&c&lThis command cannot be run by console!"));
            return true;
        }

        //  Obtain configs
        FileConfiguration config = plugin.getConfig();

        //  Get Database and prepare row variable
        DataBase DB = new DataBase(plugin);
        ResultSet userRow = null;

        Player plr = (Player) sender;

        //  Ensure a valid amount of arguments are passed
        if (args.length == 0) {
            //  Return a permissions error if they can't see the help menu
            if (!plr.hasPermission("watchhill.watchlist.help")) {
                plr.sendMessage(Utils.returnPermsErrorChat(plugin));
                return true;
            }
            //  Otherwise send the help menu
            sendArgsFailMessage(plr, config);
            return true;
        }

        //  Split the sub-command label from the rest of the args array
        label = args[0];
        args = Arrays.copyOfRange(args, 1, args.length);


        UUID argPlrUUID = null;
        String plrName = null;
        String reason = null;

        //  Only process player arg if it's required
        if (!(label.equalsIgnoreCase("list"))) {
            //  Check if possible player name is present
            if (args.length == 0) {
                sendArgsFailMessage(plr, config);
                return true;
            }

            //  Begin setup
            plrName = args[0];
            reason = args.length < 2 ? Utils.chat("&cNo reason provided") : StringUtils.join(Arrays.copyOfRange(args, 1, args.length), ' ');
            Player argPlr = Bukkit.getPlayer(plrName);


            //  Determine the player's UUID
            if (argPlr == null) {
                @SuppressWarnings("deprecation") OfflinePlayer offPlr = Bukkit.getOfflinePlayer(plrName);

                if (!(offPlr.hasPlayedBefore())) {
                    //  The player can't be found online or in the offline player list
                    plr.sendMessage(Utils.chat(config.getString("Prefix.Fail") + "&cInvalid player name!"));
                    return true;
                } else {
                    //  Player found in offline player list
                    argPlrUUID = offPlr.getUniqueId();
                    plrName = offPlr.getName();

                }
            } else {
                //  Player found online
                argPlrUUID = argPlr.getUniqueId();
                plrName = argPlr.getName();
            }

            //  Get the user row from the database
            try {
                userRow = DB.execute("SELECT * FROM watched_players " +
                                     "WHERE UUID = '" + argPlrUUID + "';");

                if (userRow.isClosed()) {
                    userRow = null;
                }
            } catch (SQLException err) {
                plr.sendMessage(Utils.chat(config.getString("Prefix.Fail") + "&cSomething went wrong while processing the arguments!"));
                plugin.getLogger().log(Level.WARNING, "Unable to obtain row in watched_players! " + err.getMessage());
            }
        }


        switch (label) {
            case "add":
                //  Check for permissions
                if (!(plr.hasPermission("watchhill.watchlist.add"))) {
                    plr.sendMessage(Utils.returnPermsErrorChat(plugin));
                    return true;
                }

                //  Prevent overwriting a pre-existing watch
                if (userRow != null) {
                    String prevReason = "Unavailable";
                    try {
                        prevReason = userRow.getString("Reason");
                    } catch (SQLException err) {
                        plr.sendMessage(Utils.chat(config.getString("Prefix.Fail") + "&cSomething went wrong while accessing the player data!"));
                        plugin.getLogger().log(Level.WARNING, "Unable to access row in watched_players! " + err.getMessage());
                    }

                    plr.sendMessage(new String[] {
                        Utils.chat(config.getString("Prefix.Fail") + "&7" + plrName + "&c is already being watched!"),
                        Utils.chat(config.getString("Prefix.Fail") + "&6Watchlist reason: &8\"&7" + prevReason + "&8\"")
                    });

                    return true;
                }

                //  Add player to the table
                try {
                    DB.execute("INSERT INTO watched_players " +
                           "VALUES('" + argPlrUUID + "', '" + plrName + "', '" + reason +"');");
                } catch (SQLException err) {
                    plr.sendMessage(Utils.chat(config.getString("Prefix.Fail") + "&cSomething went wrong while updating the watchlist!"));
                    plugin.getLogger().log(Level.WARNING, "Failed to add player to watched_players table! " + err.getMessage());

                    return true;
                }

                //  Report success
                plr.sendMessage(Utils.chat(config.getString("Prefix.Valid") + "&3Successfully added &7" + plrName + "&3 to the watchlist for: &8\"&7" + reason + "&8\""));
                break;
            case "edit":
            case "set":
            case "update":
                //  Check for permissions
                if (!(plr.hasPermission("watchhill.watchlist.edit"))) {
                    plr.sendMessage(Utils.returnPermsErrorChat(plugin));
                    return true;
                }

                //  Check for pre-existing watch
                if (userRow == null) {
                    plr.sendMessage(Utils.chat(config.getString("Prefix.Fail") + "&cCannot edit an unwatched player!"));
                    return true;
                }

                //  Check against the old reason
                String prevReason = null;
                try {
                    prevReason = userRow.getString("Reason");
                } catch (SQLException err) {
                    plugin.getLogger().log(Level.WARNING, "Unable to access row in watched_players! " + err.getMessage());
                }

                if (reason.equalsIgnoreCase(prevReason)) {
                    plr.sendMessage(Utils.chat(config.getString("Prefix.Fail") + "&cEntered reason matches previous reason!"));
                    return true;
                }
                //  Update prevReason to prevent null readout
                prevReason = (prevReason == null) ? "Unavailable" : prevReason;

                //  Update watch reason
                try {
                    DB.execute("UPDATE watched_players " +
                               "SET Reason = '" + reason + "' " +
                               "WHERE UUID = '" + argPlrUUID + "';");
                } catch (SQLException err) {
                    plr.sendMessage(Utils.chat(config.getString("Prefix.Fail") + "&cSomething went wrong while updating the watchlist!"));
                    plugin.getLogger().log(Level.WARNING, "Failed to update player in watched_players! " + err.getMessage());

                    return true;
                }

                //  Report success
                plr.sendMessage(new String[] {
                   Utils.chat(config.getString("Prefix.Valid") + "&3Successfully updated watchlist reason for &7" + plrName + "&3!"),
                   Utils.chat(config.getString("Prefix.Valid") + "&6From: &8\"&7" + prevReason + "&8\""),
                   Utils.chat(config.getString("Prefix.Valid") + "&6To: &8\"&7" + reason + "&8\"")
                });
                break;
            case "remove":
                //  Check for permissions
                if (!(plr.hasPermission("watchhill.watchlist.remove"))) {
                    plr.sendMessage(Utils.returnPermsErrorChat(plugin));
                    return true;
                }

                //  Remove the player from the watchlist
                try {
                    DB.execute("DELETE FROM watched_players " +
                               "WHERE UUID = '" + argPlrUUID + "';");
                } catch (SQLException err) {
                    plr.sendMessage(Utils.chat(config.getString("Prefix.Fail") + "&cSomething went wrong while updating the watchlist!"));
                    plugin.getLogger().log(Level.WARNING, "Failed to remove player from watched_players! " + err.getMessage());
                    return true;
                }

                //Report success
                plr.sendMessage(Utils.chat(config.getString("Prefix.Valid") + "&3Successfully removed &7" + plrName + "&3 from the watchlist!"));
                break;
            case "list":
                //  Check for permissions
                if (!(plr.hasPermission("watchhill.watchlist.list"))) {
                    plr.sendMessage(Utils.returnPermsErrorChat(plugin));
                    return true;
                }

                //  Get list limit from config
                int listLimit = config.getInt("ListLimit");

                int listLength;
                try {
                    //  Get number of watched players from DB
                    ResultSet watchedPlayers = DB.execute("SELECT COUNT(UUID) FROM watched_players;");
                    listLength = watchedPlayers.getInt("COUNT(UUID)");

                    //  Close the statement
                    watchedPlayers.getStatement().close();
                } catch (SQLException err) {
                    plr.sendMessage(Utils.chat(config.getString("Prefix.Fail") + "&cSomething went wrong while accessing the watchlist!"));
                    plugin.getLogger().log(Level.WARNING, "Failed to access watched_players! " + err.getMessage());
                    return true;
                }

                //  Get page to send
                int pageNum;
                try {
                    pageNum = args.length == 0 ? 1 : Integer.parseInt(args[0]);
                } catch (NumberFormatException err) {
                    sendArgsFailMessage(plr, config);
                    return true;
                }

                //  Check if page presents any names
                if (listLength < (pageNum - 1) * listLimit + 1) {
                    if (pageNum == 1) {
                        plr.sendMessage(Utils.chat(config.getString("Prefix.Fail") + "&cWatchlist is empty!"));
                    } else {
                        plr.sendMessage(Utils.chat(config.getString("Prefix.Fail") + "&cPage does not exist!"));
                    }
                    return true;
                }

                //  Builder for page number with click-through buttons
                ComponentBuilder comp = new ComponentBuilder("");
                //  Check if there's a previous page
                if (pageNum > 1) {
                    TextComponent backButton = new TextComponent(Utils.chat("&8[&e<&8] "));
                    backButton.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/watchlist list " + (pageNum - 1)));
                    backButton.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(Utils.chat("&3&lGo back a page!")).create()));
                    comp.append(backButton);
                }
                //  Add page number
                comp.append(new TextComponent(Utils.chat("&6Page " + pageNum)), ComponentBuilder.FormatRetention.NONE);
                //  Check if there's an additional page
                if (listLength > pageNum * listLimit) {
                    TextComponent forwardButton = new TextComponent(Utils.chat(" &8[&e>&8]"));
                    forwardButton.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/watchlist list " + (pageNum + 1)));
                    forwardButton.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(Utils.chat("&3&lGo forwards a page!")).create()));
                    comp.append(forwardButton);
                }
                //  Send page number with click-through buttons
                plr.spigot().sendMessage(comp.create());

                //  Receive names for current page
                ResultSet pagedNames;
                try {
                    //  Get the names from the DB
                    pagedNames = DB.execute("SELECT PlayerName, Reason FROM watched_players;");
                    //  Move forward to the current page (adding one to get to the start of the ResultSet)
                    for (int i=0; i<=(pageNum - 1) * listLimit; i++) {
                        pagedNames.next();
                    }

                    for (int i=0; i<listLimit; i++) {
                        //  Get the current name and watchlist reason from the DB row
                        String listName = pagedNames.getString("PlayerName");
                        String listReason = pagedNames.getString("Reason");

                        TextComponent watchName = new TextComponent(Utils.chat("&3" + listName));
                        watchName.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(Utils.chat("&6&lWatchlist reason: &8\"&7" + listReason + "&8\"")).create()));
                        plr.spigot().sendMessage(watchName);

                        if (!(pagedNames.next())) {
                            break;
                        }
                    }
                } catch (SQLException err) {
                    plr.sendMessage(Utils.chat(config.getString("Prefix.Fail") + "&cSomething went wrong while accessing page!"));
                    plugin.getLogger().log(Level.WARNING, "Failed to get name list from watched_players! " + err.getMessage());
                    return true;
                }

                break;
            default:
                //  Check for permissions
                if (!(plr.hasPermission("watchhill.watchlist.help"))) {
                    plr.sendMessage(Utils.returnPermsErrorChat(plugin));
                    return true;
                }

                sendArgsFailMessage(plr, config);
        }

        DB.close();
        return false;
    }

    private void sendArgsFailMessage(Player plr, FileConfiguration config) {
        plr.sendMessage(Utils.chat(config.getString("Prefix.Fail") + "&6Please enter a valid subcommand:"));

        TextComponent comp = new TextComponent(Utils.chat(config.getString("Prefix.Fail") + "&3/watchlist list &8[&7page&8]"));
        comp.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/watchlist list "));
        comp.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(Utils.chat("&3&lList all watched players!")).create()));
        plr.spigot().sendMessage(comp);

        comp = new TextComponent(Utils.chat(config.getString("Prefix.Fail") + "&3/watchlist add &8<&7player&8> &8[&7reason&8]"));
        comp.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/watchlist add "));
        comp.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(Utils.chat("&a&lAdd &3&la player to the watchlist with the given reason!")).create()));
        plr.spigot().sendMessage(comp);

        comp = new TextComponent(Utils.chat(config.getString("Prefix.Fail") + "&3/watchlist edit &8<&7player&8> &8[&7reason&8]"));
        comp.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/watchlist edit "));
        comp.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(Utils.chat("&7&lUpdate &3&la player's watchlist reason!")).create()));
        plr.spigot().sendMessage(comp);

        comp = new TextComponent(Utils.chat(config.getString("Prefix.Fail") + "&3/watchlist remove &8<&7player&8>"));
        comp.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/watchlist remove "));
        comp.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(Utils.chat("&c&lRemove &3&la player from the watchlist!")).create()));
        plr.spigot().sendMessage(comp);
    }
}

