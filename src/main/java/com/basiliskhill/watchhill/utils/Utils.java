package com.basiliskhill.watchhill.utils;

import com.basiliskhill.watchhill.Main;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;

public class Utils {

    //  Returns a colour formatted code for use in minecraft
    public static String chat (String s) {
        return ChatColor.translateAlternateColorCodes('&', s);
    }

    //  Returns the string indicating a perms error
    public static String returnPermsErrorChat(Main plugin) {
        FileConfiguration config = plugin.getConfig();

        return Utils.chat(config.getString("Prefix.Fail") + config.getString( "Msgs.Perm-Err"));
    }

}
