package com.basiliskhill.watchhill.utils;

import com.basiliskhill.watchhill.Main;

import java.sql.*;
import java.util.logging.Level;

public class DataBase {

    private Connection conn = null;
    private final Main plugin;

    //  Set up DB
    public DataBase(Main plugin) {
        this.plugin = plugin;

        try {
            conn = DriverManager.getConnection("jdbc:sqlite:" + plugin.getDataFolder() + "/" + plugin.getConfig().getString("DatabaseName"));
        } catch (SQLException err) {
            plugin.getLogger().log(Level.WARNING, "Failed to load database! " + err.getMessage());
        }
    }
    //  Overload to allow for exception to be handled without the need for try...catch
    public DataBase(Main plugin, Level exceptionLevel) {
        this.plugin = plugin;

        try {
            conn = DriverManager.getConnection("jdbc:sqlite:" + plugin.getDataFolder() + "/" + plugin.getConfig().getString("DatabaseName"));
        } catch (SQLException err) {
            plugin.getLogger().log(exceptionLevel, "Failed to load database! " + err.getMessage());
        }
    }

    //  Execute SQL
    public ResultSet execute(String sql) throws SQLException{
        try {
            //  Set up the statement
            Statement statement = conn.createStatement();
            statement.setQueryTimeout(30);

            //  Execute SQL and return the result
            statement.execute(sql);
            return statement.getResultSet();
        } catch (SQLException err) {
            plugin.getLogger().log(Level.WARNING, "Failed to execute SQL: " + err.getMessage());
            throw err;
        }
    }
    //  Overload to allow for exception to be handled without the need for try...catch
    public ResultSet execute(String sql, Level exceptionLevel){
        try {
            //  Set up statement
            Statement statement = conn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            statement.setQueryTimeout(30);

            //  Execute SQL and return the result
            statement.execute(sql);
            return statement.getResultSet();
        } catch (SQLException err) {
            plugin.getLogger().log(exceptionLevel, "Failed to execute SQL: " + err.getMessage());
        }

        return null;
    }

    //  Close the connection
    public void close() {
        try {
            conn.close();
        } catch (SQLException err) {
            plugin.getLogger().log(Level.WARNING, "Failed to close database connection! " + err.getMessage());
        }
    }
}
