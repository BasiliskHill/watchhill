package com.basiliskhill.watchhill;

import com.basiliskhill.watchhill.commands.IDCommand;
import com.basiliskhill.watchhill.commands.WatchListCommand;
import com.basiliskhill.watchhill.listeners.JoinListener;
import com.basiliskhill.watchhill.utils.DataBase;
import org.bukkit.plugin.java.JavaPlugin;

import java.sql.SQLException;
import java.util.logging.Level;

public class Main extends JavaPlugin {

    @Override
    public void onEnable() {
        //  Initialise each config file
        saveDefaultConfig();

        //  Initialise commands
        new IDCommand(this);
        new WatchListCommand(this);

        //  Initialise listeners
        new JoinListener(this);

        //  Initialise database
        DataBase DB = null;
        try {
            DB = new DataBase(this);
            DB.execute("CREATE TABLE IF NOT EXISTS watched_players (" +
                       "UUID varchar(64) NOT NULL," +
                       "PlayerName varchar(16) NOT NULL," +
                       "Reason varchar(256)," +
                       "PRIMARY KEY(UUID)" +
                       ");");
        } catch (SQLException err) {
            getLogger().log(Level.SEVERE, "Failed to initialise database, disabling! " + err.getMessage());
            getPluginLoader().disablePlugin(this);
        }
        finally {
            if (DB != null) {
                DB.close();
            }
        }

    }

}
