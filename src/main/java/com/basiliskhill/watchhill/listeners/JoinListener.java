package com.basiliskhill.watchhill.listeners;

import com.basiliskhill.watchhill.Main;
import com.basiliskhill.watchhill.utils.DataBase;
import com.basiliskhill.watchhill.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

public class JoinListener implements Listener {

    private final Main plugin;

    //  Initialise listener
    public JoinListener(Main plugin) {
        this.plugin = plugin;

        Bukkit.getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        //  Get the DB
        DataBase DB = new DataBase(plugin, Level.WARNING);

        //  Get the required variables
        Player plr = event.getPlayer();
        String plrName = plr.getName();
        String plrUUID = plr.getUniqueId().toString();

        //  Get the row according to the UUID
        ResultSet userRow;
            userRow = DB.execute("SELECT * FROM watched_players " +
                                           "WHERE UUID = '" + plrUUID + "';", Level.WARNING);

        //  If the row returned anything
        if (userRow != null) {
            //  Get data from the row
            String formerName = null;
            String watchReason = null;

            try {
                formerName = userRow.getString("PlayerName");
                watchReason = userRow.getString("Reason");
            } catch (SQLException err) {
                plugin.getLogger().log(Level.WARNING, "Unable to access row in watched_players! " + err.getMessage());
            }

            if (!(plrName.equalsIgnoreCase(formerName))) {
                formerName = "&8 (&cFormerly &7" + formerName + "&8)&r";
            } else {
                formerName = "";
            }
            //  Broadcast player info
            Bukkit.broadcast(Utils.chat("&cWatched player &7" + plrName + formerName + "&c has joined the server!"), "watchhill.notify");
            Bukkit.broadcast(Utils.chat("&6Watchlist reason: &8\"&7" + watchReason + "&8\""), "watchhill.notify");

        }

        DB.close();
    }
}
